/*
    GS Implementation on C6678 EVM

    Authored 2019. Youchao Wang.

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    *   Redistributions of source code must retain the above
        copyright notice, this list of conditions and the following
        disclaimer.

    *   Redistributions in binary form must reproduce the above
        copyright notice, this list of conditions and the following
        disclaimer in the documentation and/or other materials
        provided with the distribution.

    *   Neither the name of the author nor the names of its
        contributors may be used to endorse or promote products
        derived from this software without specific prior written
        permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/

/*  Include files std   */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <c6x.h>
#include <limits.h>
#include <time.h>

/*  Include files TI    */
#include <ti/dsplib/dsplib.h>
#include <ti/mathlib/mathlib.h>
#include "include/DSPF_sp_ifftSPxSP_cn.h"
#include "include/DSPF_sp_fftSPxSP_cn.h"
//#include "include/DSP_ifft32x32.h"
//#include "include/expsp.h"
//#include "include/sqrtsp.h"

/*  Include files user  */
#include "include/image.h"

/*  Macros and defines  */
#ifndef PI
# ifdef M_PI
#  define PI M_PI
# else
#  define PI 3.14159265358979323846
# endif
#endif

/*  Externs */
extern int image[N][N]; // input image grayscale 8-bpp

/*  Globals */
float colreal[N][N]={0};
float colimag[N][N]= {0};

float phase_Holo[N][N] = {0};

float absEfield[N][N] = {0};

/* Memory alignments */
/* Tell compiler arrays are double word aligned */
#pragma DATA_ALIGN(ref_Ray, 8);
float   ref_Ray [2*N*N];
#pragma DATA_ALIGN(far_Field, 8);
float   far_Field [2*N*N];
#pragma DATA_ALIGN(e_Field, 8);
float   e_Field [2*N*N];
#pragma DATA_ALIGN(diffuser, 8);
float   diffuser [2*N*N];
#pragma DATA_ALIGN(amplitude, 8);
float   amplitude [2*N*N];

#pragma DATA_ALIGN(tmp_Field, 8);
float   tmp_Field [2*N*N];

#pragma DATA_ALIGN(temp_Mat, 8);
float   temp_Mat [2*N*N];

#pragma DATA_ALIGN(x_ref, 8);
int   x_ref [2*N];

#pragma DATA_ALIGN(x_32x32, 8);
float   x_32x32 [2*N];
#pragma DATA_ALIGN(y_32x32, 8);
float   y_32x32 [2*N];
#pragma DATA_ALIGN(w_fft, 8);
float   w_fft [2*N];
#pragma DATA_ALIGN(w_fft, 8);
float   w_ifft [2*N];

#pragma DATA_ALIGN(hologram, 8);
float   hologram [2*N*N];

#pragma DATA_ALIGN(hologram, 8);
float   phase_Init [2*N*N];

#pragma DATA_ALIGN(hologram, 8);
float   phase_Replay [2*N*N];

#pragma DATA_ALIGN(hologram_Tmp, 8);
double   hologram_Tmp [2*N*N];

#pragma DATA_ALIGN(replay_Field, 8);
double   replay_Field [2*N*N];


unsigned char brev[64] = {
    0x0, 0x20, 0x10, 0x30, 0x8, 0x28, 0x18, 0x38,
    0x4, 0x24, 0x14, 0x34, 0xc, 0x2c, 0x1c, 0x3c,
    0x2, 0x22, 0x12, 0x32, 0xa, 0x2a, 0x1a, 0x3a,
    0x6, 0x26, 0x16, 0x36, 0xe, 0x2e, 0x1e, 0x3e,
    0x1, 0x21, 0x11, 0x31, 0x9, 0x29, 0x19, 0x39,
    0x5, 0x25, 0x15, 0x35, 0xd, 0x2d, 0x1d, 0x3d,
    0x3, 0x23, 0x13, 0x33, 0xb, 0x2b, 0x1b, 0x3b,
    0x7, 0x27, 0x17, 0x37, 0xf, 0x2f, 0x1f, 0x3f
};//pointer to bit reverse table containing 64 entries

/*
    This function generates the input data and also updates the
    input data arrays used by the various FFT kernels
*/
float   x_ref_float [N];
int count=0;

void generateInput () {
    int i;
    for (i = 0; i < N; i++) {
        x_32x32[2*i] = 0;
        x_32x32[2*i + 1] = 0;
    }
    /* Normalize input image data, scale by N for FFT */
    for (i = 0; i < N; i++) {
        x_ref[i] = (double)(image[count][i]);
    }

    /* Copy the reference image input data into the input array */
    for (i = 0; i < N; i++) {

        x_32x32[2*i] = x_ref[i];
        x_32x32[2*i + 1] = 0;
    }
}

/*
 * This function generates uniformly random number, equivalent to rand(1) in Matlab
 */
double gen_rand(void)
{
    return (double)rand() / (double)RAND_MAX;
}

void tw_gen_sp_fft (float *w, int n)
{
    int i, j, k;
    //const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n >> 2; j = j << 2)
    {
        for (i = 0; i < n >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n);
            w[k + 1] = (float) cos (2 * PI * i / n);
            w[k + 2] = (float) sin (4 * PI * i / n);
            w[k + 3] = (float) cos (4 * PI * i / n);
            w[k + 4] = (float) sin (6 * PI * i / n);
            w[k + 5] = (float) cos (6 * PI * i / n);
#else
            w[k]     = (float)  cos (2 * PI * i / n);
            w[k + 1] = (float) -sin (2 * PI * i / n);
            w[k + 2] = (float)  cos (4 * PI * i / n);
            w[k + 3] = (float) -sin (4 * PI * i / n);
            w[k + 4] = (float)  cos (6 * PI * i / n);
            w[k + 5] = (float) -sin (6 * PI * i / n);
#endif
            k += 6;
        }
    }
}

void tw_gen_sp_ifft (float *w, int n)
{
    int i, j, k;
   // const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n >> 2; j = j << 2)
    {
        for (i = 0; i < n >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) -sin (2 * PI * i / n);
            w[k + 1] = (float)  cos (2 * PI * i / n);
            w[k + 2] = (float) -sin (4 * PI * i / n);
            w[k + 3] = (float)  cos (4 * PI * i / n);
            w[k + 4] = (float) -sin (6 * PI * i / n);
            w[k + 5] = (float)  cos (6 * PI * i / n);
#else
            w[k]     = (float) cos (2 * PI * i / n);
            w[k + 1] = (float) sin (2 * PI * i / n);
            w[k + 2] = (float) cos (4 * PI * i / n);
            w[k + 3] = (float) sin (4 * PI * i / n);
            w[k + 4] = (float) cos (6 * PI * i / n);
            w[k + 5] = (float) sin (6 * PI * i / n);
#endif
            k += 6;
        }
    }
}

/*
 *  Gerchberg Saxton algorithm implementation
 */
//void gen_GS_phase (float *  sandro, float *  amplitude_Real, int count, ){

//}


/*
 *   The main function that implements the example functionality.
 *
 */

void main () {

    int     col = 0, row = 0;
    int     i = 0, j = 0;
    int     loop = 0;
    int     rad = 0;


    double a_cplx, b_cplx; //previously double
    double result;
    long int element;


    FILE* fp = NULL;

    clock_t t_start, t_stop, t_overhead, t_performance;
    clock_t t_start1, t_stop1, t_performance1;
    clock_t t_start2, t_stop2, t_performance2;
    clock_t t_start3, t_stop3, t_performance3;
    clock_t t_start4, t_stop4, t_performance4;
    clock_t t_start5, t_stop5, t_performance5;

    /* Initialize timer for clock */
    TSCL= 0,TSCH=0;

    /* Compute the overhead of calling _itoll(TSCH, TSCL) twice to get timing info.  */
    t_start = _itoll(TSCH, TSCL);
    t_stop = _itoll(TSCH, TSCL);
    t_overhead = t_stop - t_start;

    /* Generate twiddle factors */

    j = 0;
    for (i = 0; i <= 31; i++)
        if ((N & (1 << i)) == 0)
            j++;
        else
            break;

    if (j % 2 == 0)
    {
        rad    = 2;
    }
    else
    {
        rad    = 4;
    }

    /* Generate the twiddle factors for ifft */
    tw_gen_sp_ifft(w_ifft, N);

    /* Generate the twiddle factors for fft */
    tw_gen_sp_fft(w_fft, N);

    t_start = _itoll(TSCH, TSCL);
    t_start1 = _itoll(TSCH, TSCL);

    /*
     * Begin the process
     */

    // A = sqrt(TargetImage)
    for (row = 0; row < N; row++)
    {
        for (j = 0, col = 0; col < 2 * N; j++, col += 2)
        {
            amplitude[col + 2 * row * N] = sqrtsp(image[row][j]); // for 2d arrays, row first then column
            amplitude[col + 1 + 2 * row * N] = 0; // amplitude is a real number matrix
        }
    }


    /* Do IFFT across each row */

    for(col = 0; col < N; col ++) //width y-direction
    {
        /* Generate the input data */
        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            x_32x32[i] = amplitude[i + 2 * j * N]; // image is real value only
            x_32x32[i + 1] = 0;
        }

        /* Call the various FFT routines */
        //DSP_ifft32x32(w_fft, N, x_32x32, y_32x32);
        DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_ifft[0], y_32x32, brev, rad, 0, N);//complex to complex
        //DSPF_dp_ifftDPxDP(N, &x_32x32[0], &w_fft[0], y_32x32, rad, 0, N);

        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            tmp_Field[col + 2 * N * j] = y_32x32[i];
            tmp_Field[col + 1 + 2 * N * j] = y_32x32[i + 1];
        }
    }

//            fp = fopen("tmpField.csv","wb");
//            for (row = 0; row < N; row++)
//            {
//                col = 42;
//                {
//                    fprintf(fp,"%f + (%f)i,", tmp_Field[col + 2 * row * N], tmp_Field[col + 1 + 2 * row * N]);
//                }
//                fprintf(fp,"\n");
//            }
//            fclose(fp);

    /* Do IFFT across each column of the output array of the previous row IFFTs */

    for(row = 0; row < N; row++) // height x-direction
    {

       for (j = 0; j < 2 * N; j += 2)
       {
             x_32x32[j] = tmp_Field[j + 2 * N * row];
             x_32x32[j + 1] = tmp_Field[j + 1 + 2 * N * row];
        }
        /* Call the various FFT routines */
        //DSP_ifft32x32(w_fft, N, x_32x32, y_32x32);
        DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_ifft[0], y_32x32, brev, rad, 0, N); //
       //DSPF_dp_ifftDPxDP(N, &x_32x32[0], &w_fft[0], y_32x32, rad, 0, N);

        for (j = 0; j < 2 * N; j += 2)
        {
            far_Field[j + 2 * N * row] = y_32x32[j];
            far_Field[j + 1 + 2 * N * row] = y_32x32[j + 1];
        }

    }

    // initial phase of the image
    float angle = 0;
    for (row = 0; row < N; row++)
    {
        for (j = 0, col = 0; col < N; j += 2, col++)
        {
            angle = atan2sp(far_Field[j + 1 + 2 * N * row], far_Field[j + 2 * N * row]); //atan2(imag, real)
            phase_Init[j + 2 * N * row] = (float)cos(angle);
            phase_Init[j + 1 + 2 * N * row] = (float)sin(angle);
        }
    }

    t_stop1 = _itoll(TSCH, TSCL);
    t_start2 = _itoll(TSCH, TSCL);

    for (loop = 0; loop < 1; loop++){

    /* Do FFT across each row  */
    for(col = 0; col < 2 * N; col += 2) //width y-direction
    {
        /* Generate the input data */
        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            x_32x32[i] = phase_Init[col + 2 * N * j];
            x_32x32[i + 1] = phase_Init[col + 1 + 2 * N * j];
        }

        /* Call the various FFT routines */
        //DSP_ifft32x32(w_fft, N, x_32x32, y_32x32);
        DSPF_sp_fftSPxSP(N, &x_32x32[0], &w_fft[0], y_32x32, brev, rad, 0, N);//complex to complex
        //DSPF_dp_fftDPxDP(N, &x_32x32[0], &w_fft[0], y_32x32, rad, 0, N);

        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            tmp_Field[col + 2 * N * j] = y_32x32[i];
            tmp_Field[col + 1 + 2 * N * j] = y_32x32[i + 1];
        }
    }

 //            fp = fopen("tmpField.csv","wb");
 //            for (row = 0; row < N; row++)
 //            {
 //                col = 42;
 //                {
 //                    fprintf(fp,"%f + (%f)i,", tmp_Field[col + 2 * row * N], tmp_Field[col + 1 + 2 * row * N]);
 //                }
 //                fprintf(fp,"\n");
 //            }
 //            fclose(fp);

     /* Do FFT across each column*/

    for(row = 0; row < N; row++) // height x-direction
    {

       for (j = 0; j < 2 * N; j += 2)
       {
             x_32x32[j] = tmp_Field[j + 2 * N * row];
             x_32x32[j + 1] = tmp_Field[j + 1 + 2 * N * row];
        }
        /* Call the various FFT routines */
        //DSP_ifft32x32(w_fft, N, x_32x32, y_32x32);
        DSPF_sp_fftSPxSP(N, &x_32x32[0], &w_fft[0], y_32x32, brev, rad, 0, N); //
       //DSPF_dp_fftDPxDP(N, &x_32x32[0], &w_fft[0], y_32x32, rad, 0, N);

        for (j = 0; j < 2 * N; j += 2)
        {
            replay_Field[j + 2 * N * row] = y_32x32[j];
            replay_Field[j + 1 + 2 * N * row] = y_32x32[j + 1];
        }

    }

    t_stop2 = _itoll(TSCH, TSCL);
    t_start3 = _itoll(TSCH, TSCL);

    for (row = 0; row < N; row++)
    {
        for (j = 0, col = 0; col < N; j += 2, col++)
        {
            angle = atan2sp(replay_Field[j + 1 + 2 * N * row], replay_Field[j + 2 * N * row]);
            phase_Replay[j + 2 * N * row] = (float)cos(angle);
            phase_Replay[j + 1 + 2 * N * row] = (float)sin(angle);
        }
    }



    for (row = 0; row < N; row++)
    {
        for (col = 0; col < 2 * N; col += 2)
        {
            element = col + 2 * row * N;
            a_cplx = _amemd8((void*)&amplitude[element]); // load real and imag parts
            b_cplx = _amemd8((void*)&phase_Replay[element]);

            result = _complex_mpysp(a_cplx, b_cplx);

            temp_Mat[element] = -_hif(result);
            temp_Mat[element + 1] = _lof(result);// turns out that temp_Mat is NaN? this problem is now solved
            // has to be: a_ and b_ are double, amplitude, hologram and temp_Mat are float...
        }
    }

    t_stop3 = _itoll(TSCH, TSCL);
    t_start4 = _itoll(TSCH, TSCL);

    //tw_gen_sp_ifft(w_ifft, N);

    /* Do IFFT across each row */

    for(col = 0; col < 2 * N; col += 2) //width y-direction
    {
        /* Generate the input data */
        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            x_32x32[i] = temp_Mat[col + 2 * N * j];
            x_32x32[i + 1] = temp_Mat[col + 1 + 2 * N * j];
        }

        /* Call the various FFT routines */
        //DSP_ifft32x32(w_fft, N, x_32x32, y_32x32);
        DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_ifft[0], y_32x32, brev, rad, 0, N);//complex to complex
        //DSPF_dp_ifftDPxDP(N, &x_32x32[0], &w_fft[0], y_32x32, rad, 0, N);

        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            tmp_Field[col + 2 * N * j] = y_32x32[i];
            tmp_Field[col + 1 + 2 * N * j] = y_32x32[i + 1];
        }
    }

//            fp = fopen("tmpField.csv","wb");
//            for (row = 0; row < N; row++)
//            {
//                col = 42;
//                {
//                    fprintf(fp,"%f + (%f)i,", tmp_Field[col + 2 * row * N], tmp_Field[col + 1 + 2 * row * N]);
//                }
//                fprintf(fp,"\n");
//            }
//            fclose(fp);

    /* Do IFFT across each column of the output array of the previous row IFFTs */

    for(row = 0; row < N; row++) // height x-direction
    {

       for (j = 0; j < 2 * N; j += 2)
       {
             x_32x32[j] = tmp_Field[j + 2 * N * row];
             x_32x32[j + 1] = tmp_Field[j + 1 + 2 * N * row];
        }
        /* Call the various FFT routines */
        //DSP_ifft32x32(w_fft, N, x_32x32, y_32x32);
        DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_ifft[0], y_32x32, brev, rad, 0, N); //
       //DSPF_dp_ifftDPxDP(N, &x_32x32[0], &w_fft[0], y_32x32, rad, 0, N);

        for (j = 0; j < 2 * N; j += 2)
        {
            far_Field[j + 2 * N * row] = y_32x32[j];
            far_Field[j + 1 + 2 * N * row] = y_32x32[j + 1];
        }

    }

    t_stop4 = _itoll(TSCH, TSCL);
    t_start5 = _itoll(TSCH, TSCL);
    // phase of the image
    for (row = 0; row < N; row++)
    {
        for (j = 0, col = 0; col < N; j += 2, col++)
        {
            if (atan2sp(far_Field[j + 1 + 2 * N * row], far_Field[j + 2 * N * row]) > 0)
            {
                phase_Holo[row][col] = PI / 2;
                phase_Init[j + 2 * N * row] = (float)cos(PI / 2); //in radians
                phase_Init[j + 1 + 2 * N * row] = (float)sin(PI / 2);
            }
            else
            {
                phase_Holo[row][col] = - PI / 2;
                phase_Init[j + 2 * N * row] = (float)cos( - PI / 2);
                phase_Init[j + 1 + 2 * N * row] = (float)sin( - PI / 2);
            }
        }
    }
 }// this is for the large for loop

    t_stop5 = _itoll(TSCH, TSCL);

    t_stop = _itoll(TSCH, TSCL);
    t_performance = (t_stop - t_start) - t_overhead;
    t_performance1 = (t_stop1 - t_start1) - t_overhead;
    t_performance2 = (t_stop2 - t_start2) - t_overhead;
    t_performance3 = (t_stop3 - t_start3) - t_overhead;
    t_performance4 = (t_stop4 - t_start4) - t_overhead;
    t_performance5 = (t_stop5 - t_start5) - t_overhead;

//    fp = fopen("phase_Holo.csv","wb");
//        for (row = 0; row < N; row++)
//        {
//            for (col = 0; col < N; col++)
//            {
//                fprintf(fp,"%f,", phase_Holo[row][col]);
//            }
//            fprintf(fp,"\n");
//        }
//        fclose(fp);

//    for (row = 0; row < N; row++)
//    {
//        for (col = 0; col < 2 * N; col += 2)
//        {
//            absEfield[row][col] =
//                sqrtsp(e_Field[col + 2*N*row] * e_Field[col + 2*N*row]
//                + e_Field[col + 1 + 2*N*row] * e_Field[col + 1 + 2*N*row]);
//        }
//    }

    printf("t_performance = %d\n", t_performance);
    printf("t_performance1 = %d\n", t_performance1);
    printf("t_performance2 = %d\n", t_performance2);
    printf("t_performance3 = %d\n", t_performance3);
    printf("t_performance4 = %d\n", t_performance4);
    printf("t_performance5 = %d\n", t_performance5);


    printf("Done");

}
