/*
    OSPR Implementation on C6678 EVM

    Multi-core performance optimized

    Authored 2019. Youchao Wang.

    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    *   Redistributions of source code must retain the above
        copyright notice, this list of conditions and the following
        disclaimer.

    *   Redistributions in binary form must reproduce the above
        copyright notice, this list of conditions and the following
        disclaimer in the documentation and/or other materials
        provided with the distribution.

    *   Neither the name of the author nor the names of its
        contributors may be used to endorse or promote products
        derived from this software without specific prior written
        permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
    FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
    COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.
*/


/*  Include files std   */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <c6x.h>
#include <limits.h>
#include <time.h>

/*  Include files TI    */
#include <ti/dsplib/dsplib.h>
#include <ti/mathlib/mathlib.h>
#include "include/DSPF_sp_ifftSPxSP_cn.h"
#include "include/DSPF_sp_fftSPxSP_cn.h"


/*  Include files user  */
#include "include/image.h"

/*  Macros and defines  */
#ifndef PI
# ifdef M_PI
#  define PI M_PI
# else
#  define PI 3.14159265358979323846
# endif
#endif

//#define _PARALLEL_CORE // uncomment this for OpenMP multi-core

#ifndef N
    #define N 256
#endif

#define _LOOP 1 // this is used to determine the number of sub-frames

#define _RANDOMLENGTH 4003 // this is used to generate the random numbers

/*  Externs */
extern int image[N][N]; // input image grayscale 8-bpp

/*  Globals */
float colreal[N][N]={0};
float colimag[N][N]= {0};

float absEfield[N][N] = {0};

/* Memory alignments */
/* Tell compiler arrays are double word aligned */
#pragma DATA_ALIGN(ref_Ray, 8);
float   ref_Ray [2]; // only 2 values needed
#pragma DATA_ALIGN(far_Field, 8);
float   far_Field [2*N*N];
#pragma DATA_ALIGN(e_Field, 8);
float   e_Field [2*N*N];
#pragma DATA_ALIGN(diffuser, 8);
float   diffuser [2*N*N];
#pragma DATA_ALIGN(amplitude, 8);
float   amplitude [2*N*N];

#pragma DATA_ALIGN(tmp_Field, 8);
float   tmp_Field [2*N*N];

//#pragma DATA_ALIGN(temp_Mat, 8);
//float   temp_Mat [2*N*N];

#pragma DATA_ALIGN(x_32x32, 8);
float   x_32x32 [2*N];
#pragma DATA_ALIGN(y_32x32, 8);
float   y_32x32 [2*N];
#pragma DATA_ALIGN(w_32x32, 8);
float   w_32x32 [2*N];

// For LUT implementation
#pragma DATA_ALIGN(randomPhase, 8);
float   randomPhase [4003];

#pragma DATA_ALIGN(hologram, 8);
float   hologram [2*N*N];


unsigned char brev[64] = {
    0x0, 0x20, 0x10, 0x30, 0x8, 0x28, 0x18, 0x38,
    0x4, 0x24, 0x14, 0x34, 0xc, 0x2c, 0x1c, 0x3c,
    0x2, 0x22, 0x12, 0x32, 0xa, 0x2a, 0x1a, 0x3a,
    0x6, 0x26, 0x16, 0x36, 0xe, 0x2e, 0x1e, 0x3e,
    0x1, 0x21, 0x11, 0x31, 0x9, 0x29, 0x19, 0x39,
    0x5, 0x25, 0x15, 0x35, 0xd, 0x2d, 0x1d, 0x3d,
    0x3, 0x23, 0x13, 0x33, 0xb, 0x2b, 0x1b, 0x3b,
    0x7, 0x27, 0x17, 0x37, 0xf, 0x2f, 0x1f, 0x3f
};//pointer to bit reverse table containing 64 entries

/*
 * This function generates uniformly random number, equivalent to rand(1) in Matlab
 */
float gen_rand(void)
{
    return (float)rand() / (float)RAND_MAX;
}

/*
 *  This function is used to generate the Look Up Table
 */
void gen_randLUT(void)
{
    int i;
    for (i = 0; i < _RANDOMLENGTH; i++)
    {
        randomPhase[i] = gen_rand();
    }
}

void tw_gen_sp_fft (float *w, int n)
{
    int i, j, k;
    //const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n >> 2; j = j << 2)
    {
        for (i = 0; i < n >> 2; i += j)
        {

            w[k]     = (float) sin (2 * PI * i / n);
            w[k + 1] = (float) cos (2 * PI * i / n);
            w[k + 2] = (float) sin (4 * PI * i / n);
            w[k + 3] = (float) cos (4 * PI * i / n);
            w[k + 4] = (float) sin (6 * PI * i / n);
            w[k + 5] = (float) cos (6 * PI * i / n);

            k += 6;
        }
    }
}

void tw_gen_sp_ifft (float *w, int n)
{
    int i, j, k;
   // const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n >> 2; j = j << 2)
    {
        for (i = 0; i < n >> 2; i += j)
        {

            w[k]     = (float) -sin (2 * PI * i / n);
            w[k + 1] = (float)  cos (2 * PI * i / n);
            w[k + 2] = (float) -sin (4 * PI * i / n);
            w[k + 3] = (float)  cos (4 * PI * i / n);
            w[k + 4] = (float) -sin (6 * PI * i / n);
            w[k + 5] = (float)  cos (6 * PI * i / n);

            k += 6;
        }
    }
}

void patition_frame_col(int frame, int rad, float *  x_32x32, float *  w_32x32, float *  y_32x32)
{
    int     lowRangeCol = frame * N/8;
    int     highRangeCol = (frame + 1) * N / 8;
    int     col, i, j, element;

    /* Do IFFT across each row of the previous 2D-FFT output array */
    for(col = 2 * lowRangeCol; col <  2 * highRangeCol; col += 2) //width y-direction
    {
        /* Generate the input data */
        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            element = col +  2 * N * j;
            x_32x32[i] = e_Field[element];
            x_32x32[i + 1] = e_Field[element + 1];
        }

        /* Call the various FFT routines */
        DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_32x32[0], y_32x32, brev, rad, 0, N);//complex to complex

        for (i = 0, j = 0; j < N; i += 2, j++)
        {
            element = col + 2 * j * N;
            tmp_Field[element] = y_32x32[i];
            tmp_Field[element + 1] = y_32x32[i + 1]; // the same effect of ifft(ifftshift(Efield(:, col))) in matlab, kind of?
        }
    }

}

void patition_frame_row(int frame, int rad, float *  x_32x32, float *  w_32x32, float *  y_32x32)
{
    int     lowRangeRow = frame * N/8;
    int     highRangeRow = (frame + 1) * N / 8;
    int     row, j, element;

    /* Do IFFT across each column of the output array of the previous row IFFTs */
     for(row = lowRangeRow; row < highRangeRow; row++) // height x-direction
     {

        for (j = 0; j < 2 * N; j += 2)
        {
            element =  j +  2 * N * row;
            x_32x32[j] = tmp_Field[element];
            x_32x32[j + 1] = tmp_Field[element + 1];
         }

        /* Call the various FFT routines */
         DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_32x32[0], y_32x32, brev, rad, 0, N);

         for (j = 0; j < 2 * N; j += 2)
         {
             element =  j +  2 * N * row;
             far_Field[element] = y_32x32[j];
             far_Field[element + 1] = y_32x32[j + 1];
         }
    }

}


void task_FFT(int rad, float *  x_32x32, float *  w_32x32, float *  y_32x32)
{
#pragma omp parallel num_threads(8)
#pragma omp single
        {   // block executed only once by a single thread
            // first sub-frame
                patition_frame_col(0, rad, x_32x32, w_32x32, y_32x32);
#pragma omp task // second sub-frame
            {
                patition_frame_col(1, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // third sub-frame
            {
                patition_frame_col(2, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // forth sub-frame
            {
                patition_frame_col(3, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // fifth sub-frame
            {
                patition_frame_col(4, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // sixth sub-frame
            {
                patition_frame_col(5, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // seventh sub-frame
            {
                patition_frame_col(6, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // eighth sub-frame
            {
                patition_frame_col(7, rad, x_32x32, w_32x32, y_32x32);
            }
//#pragma omp taskwait // wait until the threads are finished
            // do nothing, make sure all are done.
            //printf("Final Eight and N = %d\n", N);
        }// End of parallel region


#pragma omp parallel num_threads(8)
#pragma omp single
        {   // block executed only once by a single thread
            // first sub-frame
                patition_frame_row(0, rad, x_32x32, w_32x32, y_32x32);
#pragma omp task // second sub-frame
            {
                patition_frame_row(1, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // third sub-frame
            {
                patition_frame_row(2, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // forth sub-frame
            {
                patition_frame_row(3, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // fifth sub-frame
            {
                patition_frame_row(4, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // sixth sub-frame
            {
                patition_frame_row(5, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // seventh sub-frame
            {
                patition_frame_row(6, rad, x_32x32, w_32x32, y_32x32);
            }
#pragma omp task // eighth sub-frame
            {
                patition_frame_row(7, rad, x_32x32, w_32x32, y_32x32);
            }
//#pragma omp taskwait // wait until the threads are finished
            // do nothing, make sure all are done.
            //printf("Final Eight and N = %d\n", N);
        }// End of parallel region
}


  void main(){

////    #pragma DATA_ALIGN(x_32x32, 8);
//    float   x_32x32 [2*N];
////    #pragma DATA_ALIGN(y_32x32, 8);
//    float   y_32x32 [2*N];
////    #pragma DATA_ALIGN(w_32x32, 8);
//    float   w_32x32 [2*N];
    int phase_Holo[N][N] = {0};

      FILE* fp = NULL;

    int     col = 0, row = 0;
    int     i = 0, j = 0;
    int     loop = 0; // i dont need this right now
    int     rad = 0;

    double a_cplx, b_cplx; //previously double
    double result;
    long int element;

    clock_t t_start, t_stop, t_overhead, t_performance;
    clock_t t_start1, t_stop1, t_performance1;
    clock_t t_start2, t_stop2, t_performance2;
    clock_t t_start3, t_stop3, t_performance3;
    clock_t t_start4, t_stop4, t_performance4;

    /* Initialize timer for clock */
    TSCL= 0,TSCH = 0;

    /* Compute the overhead of calling _itoll(TSCH, TSCL) twice to get timing info.  */
    t_start = _itoll(TSCH, TSCL);
    t_stop = _itoll(TSCH, TSCL);
    t_overhead = t_stop - t_start;

    /* Generate twiddle factors */

    j = 0;
    for (i = 0; i <= 31; i++)
        if ((N & (1 << i)) == 0)
            j++;
        else
            break;

    if (j % 2 == 0)
    {
        rad    = 2;
    }
    else
    {
        rad    = 4;
    }

    tw_gen_sp_ifft(w_32x32, N);

    // work sharing constructs
    /* Now begin the process */

    t_start = _itoll(TSCH, TSCL);
    t_start1 = _itoll(TSCH, TSCL);

    /*
     *  This randomNumberCounter is 'static', and should not be initialized
     *  every time when entering the Loop.
     */
    int   randomNumberCounter = 0;

    for(loop = 0; loop < _LOOP; loop++){  // main loop for sub-frame generation


#ifdef _PARALLEL_CORE
#pragma omp parallel for private(element, j) num_threads(8) collapse(2) schedule(auto)
#endif
   for (row = 0; row < N; row++)
   {
        for (col = 0; col < 2 * N; col += 2)
        {
            j = col / 2;
            element = col + 2 * row * N;

            // A = sqrt(TargetImage)
            amplitude[element] = sqrtsp_i(image[row][j]); // for 2d arrays, row first then column
            amplitude[element + 1] = 0; // amplitude is a real number matrix
        }
    }


   float randTargetImage = 0;

/*
 *  A modified version of random phase generation based on Look Up Table
 */
#ifdef _PARALLEL_CORE
#pragma omp parallel for private(a_cplx, b_cplx, result, element, randTargetImage, randNumberCounter) num_threads(8) collapse(2) schedule(auto)
#endif
    for (row = 0; row < N; row++)
    {
        for (col = 0; col < 2 * N; col += 2)
        {
            element = col + 2 * row * N;
            randomNumberCounter++;
            if(randomNumberCounter == _RANDOMLENGTH)
            {
                randomNumberCounter = 0;
            }

            randTargetImage = 2 * PI * randomPhase[randomNumberCounter]; //previously gen_rand(), for debug use try 0.532
            //
            diffuser[element] = cos(randTargetImage);
            diffuser[element + 1] = sin(randTargetImage);
        }
    }


#ifdef _PARALLEL_CORE
#pragma omp parallel for private(a_cplx, b_cplx, result, element) num_threads(8) collapse(2) schedule(auto)
#endif
    for (row = 0; row < N; row++)
    {
        for (col = 0; col < 2 * N; col += 2)
        {
            element = col + 2 * row * N;
            a_cplx = _amemd8((void*)&amplitude[element]); // load real and imag parts
            b_cplx = _amemd8((void*)&diffuser[element]);

            result = _complex_mpysp(a_cplx, b_cplx);

            e_Field[element] = -_hif(result);
            e_Field[element + 1] = _lof(result);
        }
    }

    t_stop1 = _itoll(TSCH, TSCL);

    t_start2 = _itoll(TSCH, TSCL); // t_start2 and t_stop2 are useless
    t_stop2 = _itoll(TSCH, TSCL);

    t_start3 = _itoll(TSCH, TSCL);

#ifdef _PARALLEL_CORE  // possibly try matrix transpose before doing IFFTs.
    task_FFT(rad, x_32x32, w_32x32, y_32x32);
#endif

#ifndef _PARALLEL_CORE

    for(col = 0; col < 2 * N; col += 2) //width y-direction
    {
         /* Generate the input data */

        // the first half after ifftshift, essentially, this works
        for (i = 0; i < N; i += 2)
        {
            j = (i / 2) + (N / 2);
            element = col + 2 * N * j;
            x_32x32[i] = e_Field[element];
            x_32x32[i + 1] = e_Field[element + 1];
        }

        // the second half after ifftshift
        for (i = N; i < 2 * N; i += 2)
        {
            j = (i / 2) - (N / 2);
            element = col + 2 * N * j;
            x_32x32[i] = e_Field[element];
            x_32x32[i + 1] = e_Field[element + 1];
        }

        /* Call the various FFT routines */
        DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_32x32[0], y_32x32, brev, rad, 0, N);//complex to complex


        for (i = 0; i < 2 * N; i += 2)
        {
            j = i / 2;
            element = col + 2 * N * j;
            tmp_Field[element] = y_32x32[i];
            tmp_Field[element + 1] = y_32x32[i + 1];
        }
    }

    for(row = 0; row < N; row++) // height x-direction
    {

        // the first half after ifftshift, essentially, this works
        for (j = 0; j < N; j += 2)
        {
            element =  j + N + 2 * N * row;
            x_32x32[j] = tmp_Field[element];
            x_32x32[j + 1] = tmp_Field[element + 1];
        }

        // the second half after ifftshift
        for (j = N; j < 2 * N; j += 2)
        {
            element =  j - N + 2 * N * row;
            x_32x32[j] = tmp_Field[element];
            x_32x32[j + 1] = tmp_Field[element + 1];
        }
        /* Call the various FFT routines */

        DSPF_sp_ifftSPxSP(N, &x_32x32[0], &w_32x32[0], y_32x32, brev, rad, 0, N); //

        for (j = 0; j < 2 * N; j += 2)
        {
            element =  j + 2 * N * row;
            far_Field[element] = y_32x32[j];
            far_Field[element + 1] = y_32x32[j + 1];
        }
    }


#endif

    t_stop3 = _itoll(TSCH, TSCL);

    t_start4 = _itoll(TSCH, TSCL);

#ifdef _PARALLEL_CORE
#pragma omp parallel for private(element, col) num_threads(8) collapse(2) schedule(auto)
#endif
    for (row = 0; row < N; row++) // binary phase
    {
        for (j = 0; j < 2 * N; j += 2)
        {
            col = j / 2;
            element = j + 2 * N * row;
            if (atan2sp(far_Field[element + 1], far_Field[element]) > 0) // angle() = atan2(imag, real);
            {
//                  phase_Holo[row][col] = PI / 2;
                phase_Holo[row][col] = 1;
            }
            else
            {
//                  phase_Holo[row][col] = - PI / 2;
                phase_Holo[row][col] = 0;
            }
        }
    }

             // frame 1
                      switch(loop){
                          case 0: fp = fopen("phase_Holo_ifftshift_frame1.csv","wb"); break;
                          case 1: fp = fopen("phase_Holo_ifftshift_frame2.csv","wb"); break;
                          case 2: fp = fopen("phase_Holo_ifftshift_frame3.csv","wb"); break;
                          case 3: fp = fopen("phase_Holo_ifftshift_frame4.csv","wb"); break;
                          case 4: fp = fopen("phase_Holo_ifftshift_frame5.csv","wb"); break;
                          case 5: fp = fopen("phase_Holo_ifftshift_frame6.csv","wb"); break;
                          case 6: fp = fopen("phase_Holo_ifftshift_frame7.csv","wb"); break;
                          case 7: fp = fopen("phase_Holo_ifftshift_frame8.csv","wb"); break;
                          default: fp = fopen("phase_Holo_ifftshift_frameNothing.csv","wb");
                      }

                      const char * comma = ',';
                              for (row = 0; row < N; row++)
                              {
                                  for (col = 0; col < N; col++)
                                  {
                                      fprintf(fp,"%d,", phase_Holo[row][col]);
//                                      fputc(phase_Holo[row][col],fp); // possibly speedup? nope doesn't work
//                                      fputs(comma, fp);
                                  }
                                  fprintf(fp,"\n");
                              }
                              fclose(fp);

                    float angle = 0;
                    uint8_t value = 0;
//
//#ifdef _PARALLEL_CORE
//#pragma omp parallel for private(element, angle, value, col) num_threads(8) collapse(2) schedule(auto)
//#endif
//                      for (row = 0; row < N; row++) // multi phase
//                      {
//                          for (j = 0; j < 2 * N; j += 2)
//                          {
//                              col = j / 2;
//                              element = j + 2 * N * row;
//                              angle = atan2sp(far_Field[element + 1], far_Field[element]);
//                              value = (uint8_t)((angle + PI)/2/PI*256);
//                              phase_Holo[row][col] = -PI + PI / 128 * (float)value;
//                          }
//                      }


    } // end of main loop for sub-frame generation

    t_stop4 = _itoll(TSCH, TSCL);
    t_stop = _itoll(TSCH, TSCL);

    t_performance = (t_stop - t_start) - t_overhead;
    t_performance1 = (t_stop1 - t_start1) - t_overhead;
    t_performance2 = (t_stop2 - t_start2) - t_overhead;
    t_performance3 = (t_stop3 - t_start3) - t_overhead;
    t_performance4 = (t_stop4 - t_start4) - t_overhead;


    printf("t_performance = %d\n", t_performance);
    printf("t_performance1 = %d\n", t_performance1);
    printf("t_performance2 = %d\n", t_performance2);
    printf("t_performance3 = %d\n", t_performance3);
    printf("t_performance4 = %d\n", t_performance4);

//                    FILE* fp = NULL;
//                         fp = fopen("tmpField2.csv","wb");
//                             for (row = 0; row < N; row++)
//                             {
//                                 for (col = 0; col < 2 * N; col+=2)
//                                 {
//                                     element = col + 2 * row * N;
//                                     fprintf(fp,"%f, %f, ", far_Field[element], far_Field[element+1]);
//                                 }
//                                 fprintf(fp,"\n");
//                             }
//                    fclose(fp);




                     /* Multi phase 256 level 8 */
//                      float angle = 0;
//                      uint8_t value = 0;


}


